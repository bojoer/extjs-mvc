Ext.define('ExtJSMVC.view.View1', {
    extend: 'Ext.window.Window',
    alias:['widget.view1'],

    height: 274,
    width: 457,
    layout: {
        type: 'fit'
    },
    title: 'My Window',

    initComponent: function() {
        var me = this;

        Ext.applyIf(me, {
            items: [
                {
                    xtype: 'gridpanel',
                    store: 'Store1',
                    columns: [
                        {
                            xtype: 'numbercolumn',
                            dataIndex: 'value',
                            text: 'Value'
                        },
                        {
                            dataIndex: 'category',
                            text: 'Category'
                        }
                    ],
                    selModel: Ext.create('Ext.selection.RowModel', {
                        mode: 'SIMPLE'
                    })
                }
            ]
        });

        me.callParent(arguments);
    }
});